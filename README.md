# Мой общий модуль для программ и скриптов
для версии `python 2.7`

[![pipeline status](https://gitlab.com/agluhov/gap27/badges/master/pipeline.svg)](https://gitlab.com/agluhov/gap27/commits/master) [![coverage report](https://gitlab.com/agluhov/gap27/badges/master/coverage.svg)](https://gitlab.com/agluhov/gap27commits/master)

Данный модуль имеет вложенную тематическую структуру и позволяет мне повторно использовать написанный мной код в различных прикладных задачах.
  
 * gap27
   * config (Работа с файлами конфигурации)
   * convert
   * filename
   * inet
     * proxy
     * requests
     * urllib2
   * parsers
   * system
   * txtfile
   * zbx

