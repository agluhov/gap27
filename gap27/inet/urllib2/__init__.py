#!/usr/bin/python
# coding: utf-8


# imports
# ------------------------------------------
import ssl
from gap27.inet.proxy import proxy_settings
from urllib2 import ProxyHandler, HTTPSHandler

# constants
# ------------------------------------------
ssl_dontverify_context = ssl.create_default_context()
ssl_dontverify_context.check_hostname = False
ssl_dontverify_context.verify_mode = ssl.CERT_NONE

https_handler = HTTPSHandler(context=ssl_dontverify_context)
proxy_handler = ProxyHandler(proxy_settings)


# procedures and functions
# ------------------------------------------


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass