#!/usr/bin/python
# coding: utf-8

from setuptools import setup, find_packages

package_dir = {'': 'gap27'}

setup(name='gap27',
      version='0.3dev',
      description='Aleksandr Gluhov python code',
      url='https://srv-glory.eurostil.ru/aleksandr.gluhov/gap27',
      author='Aleksandr Gluhov',
      author_email='aleksandr.gluhov@stroylandiya.ru',
      license='MIT',
      packages=find_packages(),
      top_level='gap27')
